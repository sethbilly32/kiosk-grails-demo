package kiosk.api.v1

import grails.rest.RestfulController
import kiosk.Product

class ProductRestController extends RestfulController {
    static namespace = 'v1'

    static responseFormats = ['json', 'xml']

    ProductRestController(){
        super(Product)
    }
}
