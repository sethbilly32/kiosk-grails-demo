package kiosk.api.v1

import grails.rest.RestfulController
import kiosk.Person

class PersonRestController extends RestfulController {
    static namespace = 'v1'
    static responseFormats = ['json', 'xml']

    PersonRestController(){
        super(Person)
    }
}
