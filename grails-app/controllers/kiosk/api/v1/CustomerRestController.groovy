package kiosk.api.v1

import grails.rest.RestfulController
import kiosk.Customer

class CustomerRestController extends RestfulController{
    static namespace = "v1"
    static responseFormats = ['json', 'xml']

    CustomerRestController() {
        super(Customer)
    }
}
