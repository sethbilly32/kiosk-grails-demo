package kiosk



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AwardController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Award.list(params), [status: OK]
    }

    @Transactional
    def save(Award awardInstance) {
        if (awardInstance == null) {
            render status: NOT_FOUND
            return
        }

        awardInstance.validate()
        if (awardInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        awardInstance.save flush:true
        respond awardInstance, [status: CREATED]
    }

    @Transactional
    def update(Award awardInstance) {
        if (awardInstance == null) {
            render status: NOT_FOUND
            return
        }

        awardInstance.validate()
        if (awardInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        awardInstance.save flush:true
        respond awardInstance, [status: OK]
    }

    @Transactional
    def delete(Award awardInstance) {

        if (awardInstance == null) {
            render status: NOT_FOUND
            return
        }

        awardInstance.delete flush:true
        render status: NO_CONTENT
    }
}
