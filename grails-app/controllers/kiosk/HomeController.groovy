package kiosk

class HomeController {

    def index() {
        render(view: '/home')
    }

    def home(){
        redirect(view: '/index')
    }
}
