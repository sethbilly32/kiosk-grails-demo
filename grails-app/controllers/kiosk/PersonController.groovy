package kiosk

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PersonController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Person.list(params), model: [personInstanceCount: Person.count()]
    }

    def show(Person personInstance) {
//        respond personInstance, view: 'show'
        render(view: 'show', model: [personInstance: personInstance])
    }


    def create() {
        [personInstance: new Person(params)]
        //respond new Person(params)
    }

    def signin(){
        render(view: 'signin')
    }

    def handleLogin(){
        def user = Person.findByUnameAndPwd(params.uname, params.pwd)
        if(user !=null){
            session.user = user
            redirect(action: 'index')
        }else{
            redirect(action: 'signin')
        }
    }

    def product(){
        redirect(action: 'index', controller: 'product')
    }

    def logout(){
        session.user = null
        session.invalidate()
        redirect(action: 'signin')
    }

    @Transactional
    def save(Person personInstance) {
        if (personInstance == null) {
            notFound()
            return
        }

        if (personInstance.hasErrors()) {
            respond personInstance.errors, view: 'create'
            return
        }

        personInstance.save flush: true
        redirect action: 'index'
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'person.label', default: 'Person'), personInstance.id])
//                redirect personInstance
//            }
//            '*' { respond personInstance, [status: CREATED] }
//        }
    }

//    def edit(Person personInstance) {
//        respond personInstance, view: 'edit'
//    }

    def edit(Person personInstance){
        render(view: 'edit', model: [personInstance: personInstance])
    }

    @Transactional
    def update(Person personInstance) {
        if (personInstance == null) {
            notFound()
            return
        }

        if (personInstance.hasErrors()) {
            respond personInstance.errors, view: 'edit'
            return
        }

        if(personInstance.save( flush: true)){
            redirect(action: 'index')
        }else{
            redirect(action: 'edit', [personInstance: personInstance])
        }



//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'Person.label', default: 'Person'), personInstance.id])
//                redirect personInstance
//            }
//            '*' { respond personInstance, [status: OK] }
//        }
    }

    @Transactional
    def delete(Person personInstance) {
        if (personInstance == null) {
            notFound()
            return
        }

        personInstance.delete flush: true
        redirect action: "index"



//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Person.label', default: 'Person'), personInstance.id])
//                redirect action: "index", method: "GET"
//            }
//            '*' { render status: NO_CONTENT }
//        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'person.label', default: 'Person'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
