package kiosk

import org.apache.commons.logging.LogFactory

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CustomerController {

    private static final log = LogFactory.getLog(this)
    def customerService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Customer.list(params), model: [customerInstanceCount: Customer.count()]
    }

    def home(){
    }

    def orders(Customer customerInstance){
        def orders = customerInstance ? OnlineOrder.findAllByCustomer(customerInstance) : []
        render ('customerorder', model: [orders: orders])
    }

    def customerOrder(Customer customerInstance){
        def orders = customerInstance ? OnlineOrder.findAllByCustomer(customerInstance) : []
        render(view: 'customerorder', model: [orders: orders, customerInstance: customerInstance])
    }

    def newOrder(){
        render(view: 'neworder', model: [productList: Product.list(params), orderItemInstance: new OrderItem(params)])
    }

    def show(Customer customerInstance) {
        render (view: 'show', model: [customerInstance: customerInstance])
    }

    def create() {
        respond new Customer(params)
    }
    def processCheckIn(Customer lookupInstance){
        def (customerInstance, welcomeMessage) = customerService.processCheckIn(lookupInstance)
        session.customerInstance = customerInstance
        //session["loggedInCustomer"] = customerInstance.firstName
        log.debug("found customer  ${customerInstance.firstName}")
        render(view: 'customerorder', model: [customerInstance: customerInstance, welcomeMessage: welcomeMessage])
    }

    def checkIn(){
        def customerInstance = new Customer(params)
        render(view: 'checkin', model: [customerInstance: customerInstance])
    }

    def delBtn(){
        redirect(action: 'index', controller: 'home')
    }

    def logout(){
        session.customerInstance = null
        session.invalidate()
    }

    def checkOut(){
        def checkedProducts = params.list('products')
        def customer = params.customer
        def customerInstance = Customer.get(customer)
        //get list of selected products, this will return only products
        def selectedProducts = Product.getAll(checkedProducts)
        def orderTotal = 0.00
        //create a new onlineorder
        //we iterate over the results in the selected product
        def onlineOrder = new OnlineOrder(orderNumber: customerService.generateOrderNumber(),
                orderDate: new Date())
        onlineOrder.save()
        for(result in selectedProducts){
            //logic to create an orderitem comes here
            def orderItem = new OrderItem()
            orderItem.product = result
            orderItem.qty = params.qty
            orderItem.total = params.total

            //we get sum of each order item in list
            orderTotal =+ params.total
            //we have save orderitem
            orderItem.save()
            onlineOrder.addToOrderItems(orderItem)
        }

        onlineOrder.orderTotal = orderTotal
        onlineOrder.save(flush: true)

        //save onlineorder to customer
        customerInstance.addToOnlineOrders(onlineOrder)
        customerInstance.save(flush: true)
        redirect(action: 'customerOrder')
    }


    @Transactional
    def save(Customer customerInstance) {
        if (customerInstance == null) {
            notFound()
            return
        }

        if (customerInstance.hasErrors()) {
            respond customerInstance.errors, view: 'create'
            return
        }

        customerInstance.save flush: true
        redirect(action: 'index')
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'customer.label', default: 'Customer'), customerInstance.id])
//                redirect customerInstance
//            }
//            '*' { respond customerInstance, [status: CREATED] }
//        }
    }

    def edit(Customer customerInstance) {
        render(view: 'edit', model: [customerInstance: customerInstance])
    }

    @Transactional
    def update(Customer customerInstance) {
        if (customerInstance == null) {
            notFound()
            return
        }

        if (customerInstance.hasErrors()) {
            respond customerInstance.errors, view: 'edit'
            return
        }

        customerInstance.save flush: true
        redirect(action: 'index')
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'Customer.label', default: 'Customer'), customerInstance.id])
//                redirect customerInstance
//            }
//            '*' { respond customerInstance, [status: OK] }
//        }
    }

    @Transactional
    def delete(Customer customerInstance) {

        if (customerInstance == null) {
            notFound()
            return
        }

        customerInstance.delete flush: true
        redirect(action: 'index')
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Customer.label', default: 'Customer'), customerInstance.id])
//                redirect action: "index", method: "GET"
//            }
//            '*' { render status: NO_CONTENT }
//        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
