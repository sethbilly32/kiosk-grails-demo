class UrlMappings {

	static mappings = {

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:'/index1')
//        "/**"(view:'/index1')

        "500"(view:'/error')
        "/api/product"(resources: "productRest",namespace:'v1')
        "/api/person"(resources: "personRest",namespace:'v1')
        "/api/customer"(resources: "customerRest", namespace:'v1')
	}
}
