import kiosk.Customer
import kiosk.Person
import kiosk.Product


class BootStrap {

    def init = { servletContext ->

        //initialise some dummy data
       [ new Product(name: 'Morning Blend', price: 9.50, sku: 'MB101'),
        new Product(name: 'Brazil Coffee', price: 5.50, sku: 'BC201'),
        new Product(name: 'Mexican Double', price: 5.50, sku: 'MD501'),
        new Product(name: 'Putorico Angaza', price: 5.50, sku: 'PA301')]*.save(failOnError: true)

        [new Customer(firstName: 'Mike', lastName:'Whelan', email:'sirbillbones@yahoo.com', phone:'0245184371', totalPoints: 3),
        new Customer(firstName: 'Russel', lastName:'Crow', email:'exampledemo@yahoo.com', phone:'0266824444', totalPoints: 2),
        new Customer(firstName: 'Bill', lastName:'Grant', email:'billgrant105@example.com', phone:'0243174549', totalPoints: 5)]*.save(failOnError: true)


        [new Person(firstname: 'Isaac', lastname: 'Cromwell', email: 'ikecromwell@github.com', uname: 'ikecromwell', pwd: 'ikecromwell10\5'),
        new Person(firstname: 'Joan', lastname: 'Arc', email: 'joanarc@hotmail.com', uname: 'joanarc', pwd: 'ja1234'),
        new Person(firstname: 'Sarah', lastname: 'Cox', email: 'dedenarkie@yahoo.com', uname: 'scoxmama', pwd: 'scoxmama'),
        new Person(firstname: 'Lois', lastname: 'Clarke', email: 'loisclarke@gmail.com', uname: 'loisclarke', pwd: 'ashaina84')]*.save(failOnError: true)
    }

    def destroy = {
    }
}
