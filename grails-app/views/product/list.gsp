<table class="table-bordered table table-responsive">
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>SKU</th>
            <th>Actions</th>
        </tr>
        <g:each in="${productInstanceList}" var="productInstance">
            <tr>
                <td>${productInstance.name}</td>
                <td>${productInstance.price}</td>
                <td>${productInstance.sku}</td>
                <td><g:link action="show" controller="product" id="${productInstance.id}" class="btn btn-sm btn-info">Select</g:link>
                </td>
            </tr>
        </g:each>
</table> 