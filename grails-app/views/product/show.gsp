
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="home">
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Product Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label" for="name">Product Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="text-input" name="name" value="${productInstance?.name}" readonly="true"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price" class="col-sm-2 control-label" for="price">Price</label>
                    <div class="col-sm-10">
                        <input type="text" class="text-input" name="price" value="${productInstance?.price}" readonly="true"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="sku" class="col-sm-2 control-label" for="sku">SKU</label>
                    <div class="col-sm-10">
                        <input type="text" class="text-input" id="sku" name="sku" value="${productInstance?.sku}" readonly="true"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-10">
                    <g:form url="[resource: productInstance, action: 'delete']" method="DELETE">
                        <g:link class="btn btn-sm btn-primary" resource="${productInstance}" action="edit">Edit</g:link>
                        <g:actionSubmit value="Delete" class="btn btn-sm btn-danger"/>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>