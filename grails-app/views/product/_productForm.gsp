<div class="form-horizontal">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label" for="name">Product Name</label>
        <div class="col-sm-10">
            <input type="text" class="text-input" name="name" id="name" value="${productInstance.name}"/>
        </div>
    </div>

    <div class="form-group">
        <label for="price" class="col-sm-2 control-label" for="price">Price</label>
        <div class="col-sm-10">
            <input type="text" class="text-input" name="price" id="price" value="${productInstance.price}"/>
        </div>
    </div>

    <div class="form-group">
        <label for="sku" class="col-sm-2 control-label" for="sku">SKU</label>
        <div class="col-sm-10">
            <input type="text" class="text-input" id="sku" name="sku" value="${productInstance.sku}"/>
        </div>
    </div>
</div>