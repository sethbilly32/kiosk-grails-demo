
<html>
<head>
    <meta name="layout" content="home">
    <title>Products</title>
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Products</div>
        <div class="panel-body">
            <g:include view="product/list.gsp" />
        </div>
        <div class="panel-footer">
            <g:link class="btn btn-primary btn-block btn-sm" action="create" controller="product">New Product</g:link>
        </div>
    </div>
</body>
</html>