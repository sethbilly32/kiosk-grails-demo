
<html>
<head>
    <meta name="layout" content="home">
    <title>Product-Edit</title>
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Edit Product</div>
        <div class="panel-body">
            <g:form url="[resource: productInstance, action: 'update']" method="PUT">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Product Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="text-input" name="name" id="name" value="${productInstance?.name}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-10">
                            <input type="text" class="text-input" name="price" id="price" value="${productInstance?.price}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sku" class="col-sm-2 control-label">SKU</label>
                        <div class="col-sm-10">
                            <input type="text" class="text-input" id="sku" name="sku" value="${productInstance?.sku}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <g:actionSubmit  value="Update" class="btn btn-primary"/>
                            <g:link class="btn btn-warning ">Cancel</g:link>&nbsp;
                        </div>
                    </div>

                </div>
            </g:form>
        </div>
    </div>

</body>
</html>