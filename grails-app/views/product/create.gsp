
<html>
<head>
    <meta name="layout" content="home">
    <title>Products Create </title>
</head>

<body>
    <div class="panel panel-default">
        <g:form url="[resource: productInstance, action: 'save']" method="POST">
            <div class="panel-heading">Create New Product</div>
            <div class="panel-body">
                <g:render template="productForm" />
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <g:submitButton name="save" value="Save" class="btn btn-primary"/>
                        <g:link class="btn btn-warning ">Cancel</g:link>&nbsp;
                    </div>
                </div>
            </div>
        </g:form>
    </div>
</body>
</html>