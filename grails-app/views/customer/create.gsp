
<html>
<head>
    <meta name="layout" content="home">
    <title>Kiosk-Customer</title>
</head>

<body>
    <g:form url="[resource: customerInstance, action:'save']" method="POST">
        <div class="panel panel-default">
            <div class="panel-heading">New Customer</div>
            <div class="panel-body">
                <g:render template="customerForm" />
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-2 col-sm-offset-10">
                        <g:submitButton name="save" value="Save" class="btn btn-sm btn-primary"/>
                        <g:link action="index" class="btn btn-warning btn-sm">Cancel</g:link>
                    </div>
                </div>
            </div>
        </div>
    </g:form>
</body>
</html>