
<html>
<head>
    <title></title>
    <meta name="layout" content="home">
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Customer Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="firstName">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="firstName" class="text-input" value="${customerInstance?.firstName}" readonly="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="lastName">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="lastName" class="text-input" value="${customerInstance?.lastName}" readonly="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" fo="phone">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="text-input" value="${customerInstance?.phone}" readonly="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="text-input" value="${customerInstance?.email}" readonly="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="totalPoints">Total Points</label>
                    <div class="col-sm-10">
                        <input type="text" name="totalPoints" class="text-input" value="${customerInstance?.totalPoints}" readonly="true"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <g:form url="[resource: customerInstance, action: 'delete']" method="DELETE">
                        <g:link resource="${customerInstance}" class="btn btn-primary" action="edit">Edit</g:link>
                        <g:actionSubmit value="Delete" class="btn btn-danger"/>
                        <g:link action="index" controller="customer" class="btn btn-warning">Cancel</g:link>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>