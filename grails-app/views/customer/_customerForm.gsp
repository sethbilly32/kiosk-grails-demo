<div class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2" for="firstName">First Name</label>
        <div class="col-sm-10">
            <input type="text" name="firstName" class="text-input" value="${customerInstance?.firstName}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="lastName">Last Name</label>
        <div class="col-sm-10">
            <input type="text" name="lastName" class="text-input" value="${customerInstance?.lastName}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" fo="phone">Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="text-input" value="${customerInstance?.phone}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email</label>
        <div class="col-sm-10">
            <input type="text" name="email" class="text-input" value="${customerInstance?.email}" />
        </div>
    </div>
</div>