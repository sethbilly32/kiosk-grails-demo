
<html>
<head>
    <title>Kiosk-Customer Order</title>
    <meta name="layout" content="home">
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Customer Orders</div>
        <div class="panel-body">
            <g:include view="customer/orders.gsp" />
        </div>
        <div class="panel-footer">
            <g:link controller="customer" action="newOrder" params="[customer: customerInstance.id]" class="btn btn-primary btn-block btn-sm">New Order</g:link>
        </div>
    </div>
</body>
</html>