
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="home">
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">Customers</div>
        <div class="panel-body">
            <g:include view="customer/list.gsp" />
        </div>
        <div class="panel-footer">
            <g:link class="btn btn-primary btn-sm btn-block" action="create">New Customer</g:link>
        </div>
    </div>

</body>
</html>