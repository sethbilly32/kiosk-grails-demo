<!doctype html>
<html lang="en">
    <head>
        <meta name="layout" content="home">
        <title>Kiosk-Customer Check-In</title>
       <link href="${resource(dir:'css', file:'bootstrap.min.css')}" rel="stylesheet" />
    </head>

    <body>
        <g:render template="checkinForm"/>
    </body>
</html>