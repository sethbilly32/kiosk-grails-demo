<!--form for checking in customer -->
<div class="container">
    <div class="row">
        <g:form url="[resource: customerInstance, action: 'processCheckIn']">
            <div class="col-sm-6 col-sm-offset-3">
                <g:textField name="phone" id="phone" placeholder="Enter your phone number" class="form-control" value="${customerInstance?.phone}"/>
                <!-- spacer -->
                <div class="row">
                    <h4></h4>
                </div>
                <!-- 1st set of number buttons -->
                <div class="row">
                    <div class="col-sm-4">
                        <input type="button" value="1" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="2" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="3" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                </div>

                <div class="row">
                    <h4></h4>
                </div>
                <!-- 2nd set of number buttons -->
                <div class="row">
                    <div class="col-sm-4">
                        <input type="button" value="4" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="5" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="6" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                </div>

                <div class="row">
                    <h4></h4>
                </div>
                <!-- 3rd set of number buttons -->
                <div class="row">
                    <div class="col-sm-4">
                        <input type="button" value="7" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="8" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="9" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                </div>

                <div class="row">
                    <h4></h4>
                </div>
                <!-- 4th set of number buttons -->
                <div class="row">
                    <div class="col-sm-4">
                        <g:link  class="btn btn-danger btn-block btn-lg" action="delBtn" controller="customer">Del</g:link>
                    </div>
                    <div class="col-sm-4">
                        <input type="button" value="0" class="btn btn-primary btn-block btn-lg" onclick="keyPad(this.value);"/>
                    </div>
                    <div class="col-sm-4">
                        <g:submitButton value="Go" name="Go" class="btn btn-success btn-block btn-lg" />
                    </div>
                </div>
            </div>
        </g:form>
    </div>
    <script>
        function keyPad(num){
            var txt = document.getElementById("phone").value;
            txt += num;
            document.getElementById("phone").value = txt;

        }
    </script>
</div>
