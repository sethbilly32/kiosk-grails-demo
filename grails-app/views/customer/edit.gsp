<html>
    <head>
        <meta name="layout" content="home">
    </head>
    <body>
        <g:form url="[resource: customerInstance, action:'update']" method="PUT">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Customer</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="firstName">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="firstName" class="text-input" value="${customerInstance?.firstName}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="lastName">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="lastName" class="text-input" value="${customerInstance?.lastName}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" fo="phone">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone" class="text-input" value="${customerInstance?.phone}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="text-input" value="${customerInstance?.email}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10">
                            <g:actionSubmit name="update" value="Update" class="btn btn-info"/>
                            <g:link action="index" controller="customer" class="btn btn-warning">Cancel</g:link>
                        </div>
                    </div>
                </div>
            </div>
        </g:form>
       
    </body>
</html>