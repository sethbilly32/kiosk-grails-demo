<table class="table table-bordered table-striped">
    <tr>
        <th>Order Number</th>
        <th>Order Total</th>
        <th>Order Date</th>
    </tr>
    <tbody>
        <g:if test="${orders.size() < 1}">
            No orders for customer
        </g:if>
        <g:else>
            <g:each in="${orders}" var="orderInstance">
                <tr>
                    <td>${orderInstance.orderNumber}</td>
                    <td>${orderInstance.orderTotal}</td>
                    <td>${orderInstance.orderDate}</td>
                    <td><g:link>Details</g:link></td>
                </tr>
            </g:each>
        </g:else>
    </tbody>

</table>