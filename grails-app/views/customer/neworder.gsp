
<html>
<head>
    <title>Kiosk-New Order</title>
    <meta name="layout" content="home">
    <style>
        tfoot{
            font-size: larger;
        }
    </style>
</head>

<body>
    <div class="panel panel-default">
        <div class="panel-heading">New Customer Order</div>
        <div class="panel-body">
            <div class="alert alert-info">Select Product to add to order</div>
            <table class="table table-bordered table-striped" id="newOrderTable">
                <tr>
                    <th>Select</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
                <g:each in="${productList}" var="productInstance">
                    <tr>
                        <td><g:checkBox name="products" value="${productInstance?.id}" checked="false"/></td>
                        <td>${productInstance?.name}</td>
                        <td class="price">${productInstance?.price}</td>
                        <td class="qty"><input type="number" id="itemQty" name="itemQty" value="${orderItemInstance?.qty}"/> </td>
                        <td class="total">${orderItemInstance?.total}</td>
                    </tr>
                </g:each>
                <tfoot>
                    <tr>
                        <td colspan="4">Order Total</td>
                        <td class="orderTotal"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <g:link class="btn btn-sm btn-primary">Checkout</g:link>
                    <g:link class="btn btn-sm btn-warning">Cancel</g:link>
                </div>
            </div>
        </div>
    </div>
    <!-- javascript function to update total pricing -->
    <script type="text/javascript">
        //trigger on change of qty
        $(".qty").change(function(){
            var row = $(this).closest("tr"); //Find the row
            var qty = row.find("input[name='itemQty']").val(); //Find the qty
            var price = row.find(".price").text(); //Find the price
            var total = price * qty;
            row.find(".total").text(total);
            var checked = row.find("input[name='products']");
            alert(checked);
            var sum = 0;
            if(checked.attr('checked')){
                sum+=total;
                $(".orderTotal").text(sum);
            }
        });

    </script>
</body>
</html>