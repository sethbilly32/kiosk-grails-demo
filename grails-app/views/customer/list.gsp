<table class="table table-bordered">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Phone</th>
        <th>Total Points</th>
        <th>Action</th>
    </tr>
    <g:each in="${customerInstanceList}" var="customerInstance">
        <tr>
            <td>${customerInstance.firstName}</td>
            <td>${customerInstance.lastName}</td>
            <td>${customerInstance.phone}</td>
            <td>${customerInstance.totalPoints}</td>
            <td><g:link id="${customerInstance.id}" action="show" class="btn btn-primary btn-sm">Select</g:link> | <g:link class="btn btn-success btn-sm" action="customerOrder" id="${customerInstance.id}">Orders</g:link></td>
        </tr>
    </g:each>
</table>