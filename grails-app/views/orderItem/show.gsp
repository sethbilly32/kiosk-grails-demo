
<%@ page import="kiosk.OrderItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'orderItem.label', default: 'OrderItem')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-orderItem" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-orderItem" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list orderItem">
			
				<g:if test="${orderItemInstance?.onlineOrder}">
				<li class="fieldcontain">
					<span id="onlineOrder-label" class="property-label"><g:message code="orderItem.onlineOrder.label" default="Online Order" /></span>
					
						<span class="property-value" aria-labelledby="onlineOrder-label"><g:link controller="onlineOrder" action="show" id="${orderItemInstance?.onlineOrder?.id}">${orderItemInstance?.onlineOrder?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${orderItemInstance?.product}">
				<li class="fieldcontain">
					<span id="product-label" class="property-label"><g:message code="orderItem.product.label" default="Product" /></span>
					
						<span class="property-value" aria-labelledby="product-label"><g:link controller="productRest" action="show" id="${orderItemInstance?.product?.id}">${orderItemInstance?.product?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${orderItemInstance?.qty}">
				<li class="fieldcontain">
					<span id="qty-label" class="property-label"><g:message code="orderItem.qty.label" default="Qty" /></span>
					
						<span class="property-value" aria-labelledby="qty-label"><g:fieldValue bean="${orderItemInstance}" field="qty"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${orderItemInstance?.total}">
				<li class="fieldcontain">
					<span id="total-label" class="property-label"><g:message code="orderItem.total.label" default="Total" /></span>
					
						<span class="property-value" aria-labelledby="total-label"><g:fieldValue bean="${orderItemInstance}" field="total"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:orderItemInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${orderItemInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
