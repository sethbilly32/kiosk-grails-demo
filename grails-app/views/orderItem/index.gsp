
<%@ page import="kiosk.OrderItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'orderItem.label', default: 'OrderItem')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-orderItem" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-orderItem" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="orderItem.onlineOrder.label" default="Online Order" /></th>
					
						<th><g:message code="orderItem.product.label" default="Product" /></th>
					
						<g:sortableColumn property="qty" title="${message(code: 'orderItem.qty.label', default: 'Qty')}" />
					
						<g:sortableColumn property="total" title="${message(code: 'orderItem.total.label', default: 'Total')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${orderItemInstanceList}" status="i" var="orderItemInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${orderItemInstance.id}">${fieldValue(bean: orderItemInstance, field: "onlineOrder")}</g:link></td>
					
						<td>${fieldValue(bean: orderItemInstance, field: "product")}</td>
					
						<td>${fieldValue(bean: orderItemInstance, field: "qty")}</td>
					
						<td>${fieldValue(bean: orderItemInstance, field: "total")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${orderItemInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
