<%@ page import="kiosk.OrderItem" %>



<div class="fieldcontain ${hasErrors(bean: orderItemInstance, field: 'onlineOrder', 'error')} required">
	<label for="onlineOrder">
		<g:message code="orderItem.onlineOrder.label" default="Online Order" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="onlineOrder" name="onlineOrder.id" from="${kiosk.OnlineOrder.list()}" optionKey="id" required="" value="${orderItemInstance?.onlineOrder?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: orderItemInstance, field: 'product', 'error')} required">
	<label for="product">
		<g:message code="orderItem.product.label" default="Product" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="product" name="product.id" from="${kiosk.Product.list()}" optionKey="id" required="" value="${orderItemInstance?.product?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: orderItemInstance, field: 'qty', 'error')} required">
	<label for="qty">
		<g:message code="orderItem.qty.label" default="Qty" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="qty" type="number" value="${orderItemInstance.qty}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: orderItemInstance, field: 'total', 'error')} required">
	<label for="total">
		<g:message code="orderItem.total.label" default="Total" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="total" value="${fieldValue(bean: orderItemInstance, field: 'total')}" required=""/>

</div>

