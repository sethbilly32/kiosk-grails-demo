
<html>
<head>
    <meta name="layout" content="home">
    <title></title>
</head>

<body>

    <g:form url="[resource: personInstance, action: 'update']" method="PUT">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Staff</div>
            <div class="panel-body">
                    <g:render template="staffForm"/>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <g:actionSubmit value="Update" class="btn btn-info btn-sm col-sm-2 "/>
                    <g:link controller="person" action="index" class="btn btn-warning btn-sm col-sm-2">Cancel</g:link>
                </div>
            </div>
        </div>
    </g:form>
</body>
</html>