
    <table class="table table-bordered">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        <g:each in="${personInstanceList}" var="personInstance">
            <tr>
                <td>${personInstance?.firstname}</td>
                <td>${personInstance?.lastname}</td>
                <td>${personInstance?.phone}</td>
                <td>${personInstance?.email}</td>
                %{--<td>${staff.Role}</td>--}%
                <td><g:link id="${personInstance.id}" action="show" class="btn btn-sm btn-info">Select</g:link>
                </td>
            </tr>
        </g:each>
    </table>
