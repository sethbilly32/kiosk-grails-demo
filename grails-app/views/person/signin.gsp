<!doctype html>
<html>
<head>
    <meta name="layout" content="home">
    <title>Kiosk - Signin</title>
    <link rel="stylesheet" href="${resource(dir:'stylesheets', file: 'bootstrap.min.css')}" />
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <g:form action="handleLogin" controller="person">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Staff CheckIn
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal">
                                <div class="form-group-sm">
                                    <label for="uname" class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <input name="uname"  value="${personInstance?.uname}"/>
                                    </div>
                                </div>
                                <div class="form-group-sm">
                                    <label for="pwd" class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <input name="pwd" value="${personInstance?.pwd}"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <g:submitButton name="signin" value="Sign In"/>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</body>

</html>

