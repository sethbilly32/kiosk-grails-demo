
<html>
    <head>
        <meta name="layout" content="home">
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-heading">Kiosk Staff</div>
            <div class="panel-body">
                <g:include view="person/list.gsp" />
            </div>
            <div class="panel-footer">
                <g:link class="btn btn-primary btn-sm btn-block" action="create">New Staff</g:link>
            </div>
        </div>
    </body>
</html>