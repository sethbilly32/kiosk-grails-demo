<div class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-sm-2" for="lastname">Last Name</label>
        <div class="col-sm-10">
            <input type="text" name="lastname" class="text-input" value="${personInstance.lastname}"/>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="firstname">First Name</label>
        <div class="col-sm-10">
            <input type="text"  name="firstname" class="text-input" value="${personInstance.firstname}"/>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="phone">Phone</label>
        <div class="col-sm-10">
            <input type="text" id="phone" name="phone" class="text-input" value="${personInstance.phone}"/>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email</label>
        <div class="col-sm-10">
            <input type="text" id="email" name="email" class="text-input" value="${personInstance?.email}"/>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="uname">Username</label>
        <div class="col-sm-10">
            <input type="text" id="uname" name="uname" class="text-input" value="${personInstance.uname}"/>
        </div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password</label>
        <div class="col-sm-10">
            <input type="text" id="pwd" name="pwd" class="text-input" value="${personInstance.pwd}"/>
        </div>

    </div>
</div>