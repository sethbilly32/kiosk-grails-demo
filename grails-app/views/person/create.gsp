
<html>
<head>
    <meta name="layout" content="home">
    <title></title>
</head>

<body>
    <g:form url="[resource: personInstance, action: 'save']" method="POST">
        <div class="panel-default panel">
            <div class="panel-heading">New Staff</div>
            <div class="panel-body">
                <g:render template="staffForm"/>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <g:submitButton name="save" value="Save" class="btn btn-sm btn-primary" />
                        <g:link action="index" class="btn btn-sm btn-warning">Cancel</g:link>
                    </div>
                </div>
            </div>
        </div>
    </g:form>
</body>
</html>