<html>
<head>
    <title>Staff Detail</title>
    <meta name="layout" content="home">
</head>

<body>
    <div class="panel panel-default">

            <div class="panel-heading">Staff Detail</div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="fname">First Name</label>
                        <div class="col-sm-10">
                            <input readonly="true" id="fname" value="${personInstance.firstname}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="lnane">Last Name</label>
                        <div class="col-sm-10">
                            <input readonly="true" id="lname"value="${personInstance.lastname}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone">Phone</label>
                        <div class="col-sm-10">
                            <input readonly="true" value="${personInstance.phone}" id="phone"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email</label>
                        <div class="col-sm-10">
                            <input readonly="true" value="${personInstance.email}" id="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="uname">Username</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="true" id="uname" value="${personInstance.uname}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Password</label>
                        <div class="col-sm-10">
                            <input type="password" id="pwd" readonly="true" value="${personInstance.pwd}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <g:form url="[action: 'delete', resource: personInstance]" method="DELETE">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <g:link action="edit" resource="${personInstance}" class="btn btn-sm btn-primary">Edit</g:link>
                            <g:actionSubmit value="Delete" class="btn btn-sm btn-danger"/>
                        </div>
                    </div>
                </g:form>
            </div>
    </div>
</body>
</html>