<!DOCTYPE html>
<html>
	<head>
		<asset:stylesheet src="bootstrap.min.css"/>
	</head>
		<!--navbar comes here -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapse " data-toggle="collapse" data-target="#indexNavBar">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Kiosk</a>
				</div>

				<div class="collapse navbar-collapse" id="indexNavBar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="${resource(dir: '', file: 'index.html')}" target="_blank">Angular Kiosk</a></li>
						<li><g:link controller="home">Kiosk</g:link></li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container" >
		<div class="panel panel-primary">
			<div class="jumbotron">
				Welcome to Angular JS Kiosk
			</div>
			</div>
		</div>
		</div>
	</body>
</html>
