<!doctype html>
<html>
<head>
    <title>
        <g:layoutTitle default="Kiosk"></g:layoutTitle>
    </title>
    <asset:stylesheet src="bootstrap.min.css"/>
    <asset:javascript src="jquery.js" />
    <g:layoutHead/>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapse" data-toggle="toggle" data-target="#crudNav">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${createLinkTo(uri: '/index1.gsp')}">Kiosk</a>
            </div>

            <div class="collapse navbar-collapse" id="crudNav">
                <ul class="nav navbar-nav navbar-right">
                    <li><g:link controller="customer" action="index">Customer</g:link></li>
                    <li><g:link controller="person" action="index">Staff</g:link></li>
                    <li><g:link controller="product" action="index">Product</g:link></li>
                </ul>
            </div>
        </div>
        
    </nav>
    <div class="container">
        <div class="row">
            <g:layoutBody/>
        </div>
    </div>
</body>
</html>