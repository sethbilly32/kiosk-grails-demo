<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="${resource(dir:'css', file:'bootstrap.min.css')}" />
        <script src="${resource(dir: 'js', file: 'jquery-1.11.3.js')}" />
        <script src="${resource(dir: 'javascripts', file: 'bootstrap.min.js')}"></script>
    </head>
    <body>
        <!--nav bar for customer -->
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapse" data-toggle="collapse" data-target="#customerNav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <g:link class="navbar-brand">Kiosk</g:link>

                <div class="collapse navbar-collapse" id="customerNav">
                    <ul class="nav navbar-nav navbar-right">
                        <li><g:link url="[action: 'orders', controller: 'customer']">Orders</g:link></li>
                        <li c   lass="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Welcome ${session.customerInstance?.firstName}<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><g:link url="[action:'profile', controller:'customer']">Profile</g:link></li>
                                <li><g:link>Logout</g:link></li>

                            </ul>
                        </li>


                    </ul>
                </div>
            </div>
        </nav><!-- end navbar-->
        <div class="container">
            <g:layoutBody>
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-3">
                        <div class="jumbotron">

                        </div>
                    </div>
                </div>
            </g:layoutBody>
        </div>
    </body>
</html>