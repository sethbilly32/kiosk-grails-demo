package kiosk

import grails.transaction.Transactional

@Transactional
class CustomerService {

    def serviceMethod() {

    }

    //this method checks if customer already exists and if not customer profile created
    def processCheckIn(lookupInstance){

        def customerInstance = Customer.findByPhone(lookupInstance.phone)
//        def welcomeMessage = "${customerInstance.firstName}"
//        log.info ("found customer:  ${customerInstance}")
//        //log.info "customer phone: ${customerInstance.phone}"
        //setup new customer
        if(customerInstance == null){
            customerInstance = lookupInstance
            customerInstance.firstName = "Customer"
        }

        //calculate current award points
        def welcomeMessage = ""
        def totalAwards = 0
        customerInstance.awards.each{
            totalAwards = totalAwards + it.point
        }
        customerInstance.totalPoints = totalAwards

        //create welcome message based on total points
        switch (totalAwards){
            case 5:
                welcomeMessage = "Welcome $customerInstance.firstName. This drink is on us!"
                break
            case 4:
                welcomeMessage = "Welcome $customerInstance.firstName. Your next is free!"
                break
            case 1..3:
                welcomeMessage = "Welcome $customerInstance.firstName You have ${totalAwards+1} points."
                break
            default:
                welcomeMessage = "Welcome $customerInstance.firstName for registering with kiosk!"

        }
        log.info "welcome message for customer: ${welcomeMessage}"
        //Add new award
        if(totalAwards < 5){
            customerInstance.addToAwards(new Award(
                    awardDate: new Date(),
                    type: "Purchase",
                    point: 1
            ))
        }else{
            customerInstance.addToAwards(new Award(
                    awardDate: new Date(),
                    type: "Award",
                    point: -5
            ))
        }
        customerInstance.save()
        return [customerInstance, welcomeMessage]
    }

    def generateOrderNumber(){
        def rand = new Random(12345)
        def orderNumber = (1..5).collect{ rand.nextInt(200-100+1)+100}

        return orderNumber
    }
}
