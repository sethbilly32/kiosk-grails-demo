package kiosk

class Customer {

    String firstName
    String lastName
    Long phone
    String email
    Integer totalPoints

    static hasMany = [awards: Award, onlineOrders: OnlineOrder]
    static constraints = {
        lastName (nullable: true)
        totalPoints(nullable: true)
        email(nullable: true, email: true)
    }

    static mapping = {
        onlineOrders lazy: false
    }
}
