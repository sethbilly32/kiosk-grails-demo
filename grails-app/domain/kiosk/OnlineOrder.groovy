package kiosk

class OnlineOrder {

    Float orderTotal
    Date orderDate
    Integer orderNumber

    static belongsTo = [customer: Customer]
    static hasMany = [orderItems: OrderItem]
    static constraints = {
        orderTotal(nullable: true)
    }

    static mapping = {
        orderItems lazy: false
    }
}
