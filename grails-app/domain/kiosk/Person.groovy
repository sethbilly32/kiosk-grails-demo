package kiosk

class Person {

    String firstname
    String lastname
    Long phone
    String email
    String uname
    String pwd

    static constraints = {
        phone(nullable: true)
    }
}
