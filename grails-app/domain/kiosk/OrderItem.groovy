package kiosk

class OrderItem {
    Integer qty
    Float total
    static transients = ['selected']
    Boolean selected
    static belongsTo = [onlineOrder: OnlineOrder, product: Product]

    static constraints = {
    }
}
