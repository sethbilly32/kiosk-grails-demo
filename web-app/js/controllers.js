/**
 * Created by crash on 5/11/15.
 */

'use strict';

app.controller('ProductController', function ($scope, Product, $routeParams, $location){

    //Product.query(function(data){
    //    $scope.products = data.products;
    //});
    //var product = Product.get({id:$routeParams.id}, function() {
    //    var id = product.id;
    //
    //});
    $scope.productData = new Product();
    $scope.saveProduct = function(){
        $scope.productData.$save()
        $location.path('/product');
    };
    $scope.editProduct = function(){
        Product.get({id: $routeParams.id},function(product){
            $scope.product = product;
        });
    };
    if($routeParams.id){
        $scope.editProduct();
        console.log('Editing product :'+$routeParams.id);
    }else{
        console.log('Loading product listing');
        $scope.products = Product.query();
    }

    $scope.updateProduct = function(){
        $scope.product.$put(function(){
            $location.path('/product');
        });
    };

    $scope.deleteProduct = function(product){
        product.$delete();
        $scope.products = Product.query();
    }


});

app.controller('StaffController', ['$scope', '$routeParams', '$location', 'Person', function($scope, $routeParams,
    $location, Person){
    $scope.staff = new Person();

    //query for all staff
    $scope.loadStaffList = function(){
        $scope.staffs = Person.query();
    };

    //edit a selected staff
    $scope.editStaff = function(){
        Person.get({id:$routeParams.id}, function(staff){
           $scope.staff = staff ;
        });
    };
    if($routeParams.id){
        $scope.editStaff();
    }else{
        $scope.loadStaffList();
        //$scope.staffs = Person.query();
    }

    //delete a staff
    $scope.deleteStaff = function(staff){
        staff.$delete();
        $scope.loadStaffList();
    }

    //update staff info
    $scope.updateStaff = function(){
        $scope.staff.$update(function(){
           $location.path('/staff');
        });
    }

    //create new staff
    $scope.createStaff = function(){
        $scope.staff.$save();
        $location.path('/staff');
    }
}]);

app.controller('CustomerController', ['$scope', '$routeParams', '$location', 'Customer', function($scope, $routeParams, $location, Customer){
    $scope.customer = new Customer();

    //function to query list of customers
    $scope.loadCustomerList = function(){
        $scope.customers = Customer.query();
    }

    //delete customer
    $scope.deleteCustomer = function(customer){
        customer.$delete();
        $scope.loadCustomerList();
    }

    //create new customer
    $scope.createCustomer = function(){
        $scope.customer.$save();
        $location.path('/customer');

    }
    $scope.showCustomer = function(){
        Customer.get({id: $routeParams.id}, function(customer){
            $scope.customer = customer;
        });
    }
    //show customer detail for edit
    if($routeParams.id){
        $scope.showCustomer();
    }else{
        $scope.loadCustomerList();
    }

    //update customer info
    $scope.updateCustomer = function () {
        $scope.customer.$update(function(){
            $scope.loadCustomerList();
            $location.path('/customer');
        })
    }

}]);
