/**
 * Created by crash on 5/11/15.
 */
//switching to
var kioskServices = angular.module('kioskServices', ['ngResource']);

//$resource not deserializing json array well
//switching to jsonp
kioskServices.factory('Product',['$resource', function($resource){
    return $resource('api/product/:id' +'.json', {id: '@id'}, {
        put:{method:'PUT'}
    });

}]);

kioskServices.factory('Person', ['$resource', function($resource){
    return $resource('api/person/:id'+'.json', {id:'@id'}, {
        update: {method: 'PUT'}
    });
}]);

kioskServices.factory('Customer', ['$resource', function($resource){
    return $resource('api/customer/:id'+'.json', {id: '@id'}, {
       update: {method: 'PUT'}
    });
}]);
