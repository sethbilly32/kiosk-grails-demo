/**
 * Created by crash on 5/9/15.
 */

var  app = angular.module('kioskApp', ['ngRoute', 'kioskServices']);
//app.run(function($rootScope, $location) {
//    $rootScope.goto = function (hash) {
//        $location.path(hash);
//    }
//})
app.config(function($routeProvider, $locationProvider){
    //$locationProvider.hashPrefix('!');

    $routeProvider.
        when('/product', {
            templateUrl: 'partials/product/index.html',
            controller: 'ProductController'
        })
        .when('/product/edit/:id', {
            templateUrl: 'partials/product/edit.html',
            controller: 'ProductController',
            resolve: {

            }
        })
        .when('/product/new', {
            templateUrl: 'partials/product/create.html',
            controller: 'ProductController'
        })
        .when('/product/edit/:id', {
            templateURL: 'partials/product/index.html',
            controller: 'ProductController'
        })
        .when('/product/delete/:id', {
           templateUrl: 'partials/product/index.html',
            controller: 'ProductController',
            resolve: {
                action: function(){return 'delete';}
            }
        })
        .when('/staff', {
            templateUrl: 'partials/person/index.html',
            controller: 'StaffController'
        })
        .when('/staff/edit/:id',{
            templateUrl:'partials/person/edit.html',
            controller: 'StaffController'
        })
        .when('/staff/new', {
            templateUrl: 'partials/person/person.html',
            controller: 'StaffController'
        })
        .when('/customer', {
            templateUrl:'partials/customer/index.html',
            controller: 'CustomerController'
        })
        .when('/customer/edit/:id', {
            templateUrl:'partials/customer/edit.html',
            controller: 'CustomerController'
        })
        .when('/customer/new', {
            templateUrl: 'partials/customer/customer.html',
            controller: 'CustomerController'
        });

});